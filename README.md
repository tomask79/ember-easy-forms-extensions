# Validations using Ember Easy Form Extensions with Ember CLI #

Ember JS is currently my personal number one in JavaScript frameworks. Yeah, i still didn't put my hands on React JS (shame one me!), but I get to use Ember very fast because of his structuralization and the fact that Ember leads the programmer in how the things should be done, which is what I always have been missing in Angular JS since the first versions. There are always at least three ways of doing something in AngularJS where two of them are bad...:) 

Anyway, where I was impressed by Angular JS was his Form Validation API, tons of integrated directives make the creations of forms very easy, which is on the other hand something I was missing in EmberJS until I discovered EmberJS addon called [ember-easy-form-extensions](https://github.com/sir-dunxalot/ember-easy-form-extensions/wiki). First what you need to run this demo. 

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://www.ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Check the demo at [http://localhost:4200](http://localhost:4200).

## Important parts to look on ##

In the demo I setup simple application template with just main heading and outlet where index template containing the following form is embed:


```
{{#form-wrapper}}
  {{#form-controls legend='Add new book into system'}}

	{{input-group
		property='isbn'
	}}
	
	{{input-group
		property='numberOfCopies'
	}}

    {{input-group
      	property='title'
    }}
	
	{{input-group
		property='author'
	}}

    {{input-group
      property='description'
      type='textarea'
    }}
  {{/form-controls}}
  
  {{form-submission
    saveText='SaveBook'
	cancelText='Cancel book adding'
  }}

{{/form-wrapper}}
```
To see the list of all properties on the ember-easy-form-extensions form tags, see their wiki: [form template](https://github.com/sir-dunxalot/ember-easy-form-extensions/wiki/2.-Form-Template)

Most sexy part is the JavaScript validations configuration:

Let's say **I want to have ISBN mandatory and at least 5 characters long**, so inside **validations** object list within the form mixin, you will define following object:

```
'model.isbn': {
  presence: true,
  length: { minimum: 5 }
}
```

Now let's say **I want to have number of copies field to have validated that user has entered only numbers**:


```
'model.numberOfCopies': {
    presence: true,
    numericality: { messages: { numericality: 'must be a number!' } }
},
```

You can even define **conditional validations**, like some field is mandatory only if certain condition is valid, for example for description field template would look like the following:


```
'model.description': {
    presence: {
       if: function(object, validator) {
         // Put here conditional logic determining whether description will be evaluated.
		              return true;
       }
    }	
}
```
To see the list of possible validations check wiki of [ember-validations](https://github.com/dockyard/ember-validations)

I also highly recommend page [Ember Addons](http://www.emberaddons.com/). Where you can find various addons and libraries for EmberJS. Notice that ember-easy-form-extensions addon is very popular there.

regards

Tomas