import Ember from 'ember';
import FormMixin from 'ember-easy-form-extensions/mixins/controllers/form';
import EmberValidations from 'ember-validations';

export default Ember.Controller.extend(
	EmberValidations,
	FormMixin, {

  	  validations: {
		  'model.isbn': {
			  presence: true,
        	  length: { minimum: 5 }
		  },
		  'model.numberOfCopies': {
		  	  presence: true,
			  numericality: { messages: { numericality: 'must be a number!' } }
		  },
		  'model.author': {
		  	  presence: {
				  message: 'Book author has to be entered!'
			  },
		  },
		  'model.description': {
		      presence: {
		          if: function(object, validator) {
					  // Put here conditional logic determining whether description will be evaluated.
		              return true;
		          }
		      }	
		  }
  	  },
	  
	  cancel() {
	  	  alert('Cancelling book adding!');	
	  },

  	  /* Runs after validations pass and submit button in {{form-submission}} is clicked */
  	  save () {
		  alert('Submitted book \n TITLE: '+this.get('model').title+' \n ISBN: '+this.get('model').isbn);
  	  },
});