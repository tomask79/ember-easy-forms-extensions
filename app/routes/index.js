import Ember from 'ember';

export default Ember.Route.extend({
  model : function () {
	return {
		title: 'Enter book title',
		numberOfCopies: 0,
		isbn: 'Enter book ISBN',
		author: 'Enter book author',
		description: 'Enter some senteces about book. ',
	};
  }
});

